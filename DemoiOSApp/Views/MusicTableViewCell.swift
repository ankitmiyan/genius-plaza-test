//
//  MusicTableViewCell.swift
//  DemoiOSApp
//
//  Created by Ankit on 09/07/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import UIKit

class MusicTableViewCell: UITableViewCell {

    @IBOutlet weak var mediaType: UILabel!
    @IBOutlet weak var musicName: UILabel!
    @IBOutlet weak var musicImage: UIImageView!
    @IBOutlet weak var cardView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        addShadowToCard()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func addShadowToCard(){
        cardView.layer.masksToBounds =  false
        cardView.layer.shadowColor = UIColor.darkGray.cgColor;
        cardView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        cardView.layer.shadowOpacity = 0.7
        musicImage.layer.borderColor = UIColor.black.cgColor
        musicImage.layer.borderWidth = 1
        musicImage.layer.masksToBounds =  false
        musicImage.layer.shadowColor = UIColor.darkGray.cgColor;
        musicImage.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        musicImage.layer.shadowOpacity = 0.5
    }
}
