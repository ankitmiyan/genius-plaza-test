//
//  ViewController.swift
//  DemoiOSApp
//
//  Created by Ankit on 09/07/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    // MARK:- Variable Declaration
    
    // MARK:- Override functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.perform(#selector(nextScreen), with: nil, afterDelay: 1.0)
    }
    
    // MARK:- Helper functions
    
    @objc func nextScreen(){
        self.performSegue(withIdentifier: "pushDashboard", sender: self)
    }
}

