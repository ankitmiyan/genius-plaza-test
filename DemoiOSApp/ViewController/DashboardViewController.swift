//
//  DashboardViewController.swift
//  DemoiOSApp
//
//  Created by Ankit on 09/07/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class DashboardViewController: UIViewController,UITableViewDataSource,UITableViewDelegate  {

    var feed:Feed?
    var resultResponse:[Result] = []
    
    // MARK:- Variable Declaration
    @IBOutlet weak var tableView: UITableView!
    
    // MARK:- Override functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true)
        registerForNib()
        getResponse()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }

    // MARK:- Helper functions
    
    func registerForNib(){
        tableView.register(UINib(nibName: "MusicTableViewCell", bundle: nil), forCellReuseIdentifier: "MusicTableViewCell")
    }
//    let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

    func getResponse(){
        let task = URLSession.shared.welcomeTask(with: URL(string: DemoiOSHelper.url)!) { welcome, response, error in
            if let welcome = welcome {
                DispatchQueue.main.async {
                    if let feed = welcome.feed{
                        let obj:Feed = feed
                        if let result = obj.results{
                            self.resultResponse = result
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    // MARK:- Table View functions
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultResponse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MusicTableViewCell", for: indexPath) as! MusicTableViewCell
        let result = resultResponse[indexPath.row]
        cell.mediaType.text = (result.name != nil) ? result.name : "NA"
        cell.musicName.text = (result.artistName != nil) ? result.artistName : "NA"
        if let imageURl = result.artworkUrl100 {
            cell.musicImage.sd_setImage(with: URL(string: imageURl), placeholderImage: UIImage(named: "logo"), options: .allowInvalidSSLCertificates)
        }else {
            cell.musicImage.image = UIImage(named: "logo")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
}

